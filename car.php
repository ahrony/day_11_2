<?php

class car {
    public $name ='';
    public $color ='';
    public $manufacture ='';
    public $manufacture_date ='';
    public $price ='';
    public $light ='';
    public $seat ='';
    public $fuel ='';
    public $speed ='';
    public $engine ='';
    
    public function assign($data1 = '',$data2 = '',$data3 = '',$data4 = '',$data5 = '',$data6 = ''){
	
	$this->name = $data1;
	$this->color = $data2;
	$this->manufacture = $data3;
	$this->manufacture_date = $data4;
	$this->speed = $data5;
	$this->price = $data6;
	
    }
    
    public function info(){
	
	echo "Name : ".$this->name."<br>";
	echo "Color : ".$this->color."<br>";
	echo "Manufacture Name : ".$this->manufacture."<br>";
	echo "Manufacture Date : ".$this->manufacture_date."<br>";
	echo "Speed : ".$this->speed."<br>";
	echo "Price : ".$this->price."<br>";
	
	
    }
}
